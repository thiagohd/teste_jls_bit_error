#include<stdio.h>
#include<string.h>
#include<conio.h>
#include<stdlib.h>
#include<locale.h>

#define MAX_STR 500
int lin;
int col;
int tam;



void getHearder(FILE *p);
FILE * abreArq(char *nome);

/***************************
 *
 *
 ***************************/
FILE * abreArq(char *nome)
{
	FILE *f;

	f = fopen(nome,"rb+");
	if(f == NULL)
	{
		printf("Não foi possível abrir o arquivo: %s\n",nome);
		exit(1);
	}
	return f;
}

int main(int argc, char *argv[])
{ 
	FILE *p;
	char str[50];
	char valor;
	unsigned int n, i;
	long int cont, qtde, soma, bias;
	
  printf("Starting....%d\n",argc);
	
	if(argc == 3)
  {
	  strcpy(str,argv[1]);
		valor = atoi(argv[2]);

  }else
  {
    printf("Digite o nome do arquivo, a quantidade de dados a ser escrito e o valor do pixel: ");
		scanf("%s %d %c",str,&n,&valor);
		p = abreArq(str);
		fseek (p, 0, SEEK_END);
		for(i=0; i < n; i++)
			putc(valor,p);
		fclose(p);
		return 0;
  }
	
	p = abreArq(str);
	
	//fseek(p,0,SEEK_END);
	getHearder(p);
	bias = ftell(p);
	qtde = lin * col;
	fseek (p, 0, SEEK_END);
	cont = ftell(p);
	soma = qtde - cont + bias;
	printf("bias: %ld, cont: %ld, qtde: %ld, soma: %ld, lin: %d, col: %d\n",bias, cont, qtde, soma, lin, col);
	//printf("%d %c\n",n, 49);
	printf("pos: %ld\n",cont);
	//system("pause");
	for(i=0; i < soma; i++)
		putc(valor,p);
	printf("pos: %u\n",ftell(p));
	printf("Escritos: %d bytes\n",i);
	fflush (p);   
	fclose(p);
  return 0;
} 

void getHearder(FILE *p)
{
	char linha[MAX_STR];
	char tipo[3];
	
	fgets(linha,MAX_STR,p);
	sscanf(linha,"%s",tipo);
	printf("%s\n",tipo);
	fgets(linha,MAX_STR,p);
	if(linha[0]=='#')
	{
    printf("%s\n",linha);
	  fgets(linha,MAX_STR,p);
	}
	sscanf(linha,"%d %d", &col, &lin);
	printf("%d %d\n", lin, col);
	fgets(linha,MAX_STR,p);
	sscanf(linha,"%d", &tam);
	printf("%d\n", tam);
	
}
