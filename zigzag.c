#include <stdio.h>
#include <stdlib.h>

#define max_lin 8
#define max_col 8

int mat[max_lin][max_col];
int sentido;
int l,c; // linha e coluna para o zigzag
void zigzag();
void sobe();
void desce();

void exibeMat()
{
		 int lin,col;
		 for (lin=0; lin < max_lin; lin++){
		 	  printf("|");
		 			for (col=0; col < max_col; col++){
		 				   printf(" %3d",mat[lin][col]);
		 			}
		 	  printf(" |\n");
		 }
		 	  printf("\n");	
}

void printD()
{
	  if( l >= max_lin || c >= max_col){
				  printf("violacao: l: %d c: %d \n",l,c);
				  exit(1);
			}
			printf(" %3d",mat[l][c]);
	
}

void zigzag()
{
	 l=0,c=0;
		char var[3];
	 sentido=0;
		
		printD();
		
		while( sentido < 4 )
		{
			  if (sentido == 0 ){
						
						  if ( l == 0 )
							    c++;
					   else if (l < max_lin -1)
           l++;						
					   desce();
								
					}else{
        if (l < max_lin -1)
          l++;
        else
          c++;

        sobe();								
					}
					printf("\n");
					//printf("l: %d c: %d sentido: %d \n",l,c,sentido);
			  //scanf("%s",var);
		}
}

void sobe()
{   
		  printD();
   printf(".");
	  	if(l == c){
					 sentido =5;
						return;
				}
				sentido = 0;
				while(l >= 0 && c <= max_col -1)
				{
					  l--;
							c++;
							printD();
							 printf("..");
							
							if (l == 0 || c == max_col -1){
										return;
					  }
				}
	
}

void desce()
{
	   printD();
				printf(",");
	  	if(l == c){
					 sentido = 5;
						return;
				}
	   
				sentido=1;
				while(c >= 0 && l <= max_lin -1){
					   c--;
								l++;
							printD();
							printf(",");
								if (c == 0 || l == max_lin -1){
								   return;
					   }
				}
}
	
int main ()
{
	 int vetL [64] = {0, 1, 5, 6, 14, 15, 27, 28, 2, 4, 7, 13, 16, 26, 29, 42, 3, 8, 12, 17, 25, 30, 41, 43, 9, 11, 18, 24, 31, 40, 44, 53, 10, 19, 23, 32, 39, 45, 52, 54, 20, 22, 33, 38, 46, 51, 55, 60, 21, 34, 37, 47, 50, 56, 59, 61, 35, 36, 48, 49, 57, 58, 62, 63};
	 int lin,col,i;
		for (i=0,lin=0; lin < max_lin; lin++){
					for (col=0; col < max_col; col++,i++){
						   mat[lin][col]=vetL[i];
					}
		}
		
		exibeMat();
		printf("\n\n\n Agora em zigzag: \n\n\n");
	
		zigzag();
		return 0;
}