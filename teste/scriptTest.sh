ROOT=$1
n=$2
N=7
echo "Inicio... ${ROOT} "
for b in $(seq 0 1 $N); do
  echo "-------------------------------"
  echo "${n} ${b}"
  echo "${n} ${b}" >in.txt
  ../bitError.exe ${ROOT}.jls <in.txt
  jlsd ${ROOT}.jls -o${ROOT}_${n}_${b}.pgm
  echo "Retorno: $?"
  if [ $? -eq 0 ];
    then
     echo "Com erro"
     ../preenche ${ROOT}_${n}_${b}.pgm 255
     psnr ${ROOT}.pgm ${ROOT}_${n}_${b}.pgm
  else
     echo "Sem erro!"
     psnr ${ROOT}.pgm ${ROOT}_${n}_${b}.pgm
  fi
  ../bitError.exe ${ROOT}.jls <in.txt
  
  echo "Fim... ${ROOT}"
  echo "${n} ${b}"
  echo "-------------------------------"
  echo "-------------------------------"
done