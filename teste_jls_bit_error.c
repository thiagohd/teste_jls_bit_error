#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
#include<ctype.h>
#include<locale.h>

int contaBytes(char *nome);
long long int contaBytesJls(char *nome);
void sabotadorDeBit(char *nome);
//extern FILE  *ifpa , *ofpa;



/******************************
 *
 ******************************/
int main(int argc, char *argv[])
{
	setlocale(LC_ALL,"");

  char str[150];
	printf("argc: %d....\n",argc);
  if(argc == 2)
  {
    printf("Starting....\n");
	  strcpy(str,argv[1]);
  }else
  {
      printf("Digite o nome do arquivo: ");
			scanf("%s",str);
  }
  sabotadorDeBit(str);

  return 0;
}



/**************************
 *
 *
 **************************/
int contaBytes(char *nome)
{
	FILE *p;
	int max = 500;
	int cont=0;
	int linha, coluna;
	int tam;
	char header[max];
	char tipo[3];

	p = fopen(nome,"r");
	if(p==NULL)
	{
		printf("N�o foi pass�vel abrir o arquivo: %s\n",nome);
		exit(1);
	}

	fgets(header,max,p);
	sscanf(header,"%s",tipo);
	printf("Tipo: %s\n",tipo);
	fgets(header,max,p);
	printf("comment: %s\n",header);
	fgets(header,max,p);
	if ( sscanf(header,"%d %d",&coluna,&linha) != 2 )
	{
	   //sscanf(p,"%d%d",&coluna,&linha);
		 printf("N�o foi poss�vel obter as linhas e colunas...");
		 exit(1);
	}
	fgets(header,max,p);
	printf("linha: %d\nColuna: %d\n", linha,coluna);
	sscanf(header,"%d",&tam);
	printf("tam: %d\n",tam);

	while(!feof(p))
	{
		cont++;
		fgetc(p);
	}
	cont--;
	printf("Quantidade de byte de dados: %d\n",cont);
	return cont;
}

/**************************
 *
 *
 **************************/
long long int contaBytesJls(char *nome)
{
	FILE *p;
	long long int cont=0;
	char dado;
	p = fopen(nome,"rb");
	if(p==NULL)
	{
		printf("N�o foi pass�vel abrir o arquivo: %s\n",nome);
		exit(1);
	}

	while(!feof(p))
	{
		dado = fgetc(p);
		if(cont < 24){
		  printf("pos %4lld: 0x%0x\n",cont,(char)dado);
		}
		else if(cont == 24)
			printf("\n\n");
		//else if( cont <= 1000)
		//  printf("pos %5lld: 0x%0x\n",cont,(char)dado);

		  cont++;

	}
  cont--;
	printf("\nQuantidade de byte do arquivo: %lld\nCabe�alho: 27\nDados compactados: %lld\n",cont,cont-27);
	return cont;
	fclose(p);
}

/**************************
 *
 *
 **************************/
void sabotadorDeBit(char *nome)
{
	FILE *p;
	long long int cont;
	char dado;
	char mask;
	int pos;
	long long int n;

  cont = contaBytesJls(nome);
	p = fopen(nome,"rb+");
	if(p==NULL)
	{
		printf("N�o foi pass�vel abrir o arquivo: %s\n",nome);
		exit(1);
	}
	do{
	    printf("Digite o byte (25 a %lld) e a posi��o (0 a 7) do bit: ",cont-2);
	    scanf("%lld %d",&n,&pos);
			if( (n<24 && n>cont-2) || (pos<0 && pos > 7) )
			  printf("\nValores inv�lidos...!\n");
		}while((n<24 && n>cont-2) || (pos<0 && pos > 7));

		fseek(p,n,SEEK_SET);
		dado = fgetc(p);
		mask = 1 << pos;
		printf("dado: 0x%2x \n", dado);
		printf("pos: %lld \n",pos);
		printf("mask: 0x%2x \n",mask);
		if( mask & dado )
			dado = dado & (~mask);
		else
			dado = dado | mask;
		printf("dado: 0x%2x \n", dado);
		fseek(p,n,SEEK_SET);
		fputc(dado,p);

	fclose(p);
}

/**************************
 *
 *
 **************************/
void sabotadorDeslocador(char *nome)
{
	FILE *p;
	int cont=0;

	p = fopen(nome,"r");
	if(p==NULL)
	{
		printf("N�o foi pass�vel abrir o arquivo: %s\n",nome);
		exit(1);
	}

	while(!feof(p))
	{
		cont++;
		fgetc(p);
	}
	cont--;
	printf("Quantidade de byte do arquivo: %d\nCabe�alho: 27\nDados compactados: %d",cont,cont-27);

	fclose(p);
}




